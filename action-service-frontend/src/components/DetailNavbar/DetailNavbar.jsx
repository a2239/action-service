import React, { useRef } from 'react';
import classes from "./DetailNavbar.module.css";
import logo from "../../asstes/logo.svg";
import { Link } from "react-router-dom";
import { FaBars } from 'react-icons/fa';
import { useState } from 'react';
import { url } from '../Url/Url';
import axios from 'axios';

export default function DetailNavbar({ setPayload, setLoaded }) {
    const [open, setOpen] = useState(false);
    const inputRef = useRef(null);

    const search = (e) => {
        const url1 = url() + `coupons/search?word=${inputRef.current.value}`;
        e.preventDefault();
        setLoaded(true);
        axios.get(url1)
            .then(res => {
                const persons = res.data;
                setPayload(persons?.payload);
                if (persons?.payload) {
                    setLoaded(false);
                }
            });
            inputRef.current.value = "";
    }
    return (
        <div className={open ? `${classes.navbar} ${classes.active}` : classes.navbar}>
            <div className="container">
                <div className="row">
                    <div className="col-md-6 col-sm-6 col-xs-12">
                        <div className={classes.logo_content}>
                            <Link className={classes.logo} to="/">
                                <img src={logo} alt="logo" />
                            </Link>
                            <Link to="/">Магазины</Link>
                            <div className={classes.bars_icon} onClick={() => setOpen(!open)}>
                                <FaBars />
                            </div>
                        </div>
                    </div>
                    <div className="col-md-6 col-sm-6 col-xs-12">
                        <div className={classes.search_form}>
                            <form onSubmit={search}>
                                <input type="text" ref={inputRef} placeholder='Найти магазин...' name="search_coupon"/>
                                <button type="submit">Найти</button>
                            </form>
                            <Link to="/">Магазины</Link>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
