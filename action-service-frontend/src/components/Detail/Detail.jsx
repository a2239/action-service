import React, { useState } from 'react';
import classes from "./Detail.module.css";
import { Rating } from '@mui/material';
import CircularProgress from '@mui/material/CircularProgress';

export default function Detail({ loaded, payload }) {
  const [value, setValue] = useState(4);
  
  return (
    <div className={classes.detail}>

      <div className="container">
        {payload.length ?
          <div className="row">
            <div className="col-md-3">
              <div className={classes.detail_aside}>
                <div className={classes.detail_img}>
                  <img src={!loaded ? payload[0]?.partner?.imageUrl : ""} alt="img" />
                </div>
                <div className={classes.detail_title}>
                  <h4>{!loaded ? payload[0]?.partner?.name : ""}</h4>
                  <div className={classes.rates}>
                    <Rating
                      name="simple-controlled"
                      value={value}
                      onChange={(event, newValue) => {
                        setValue(newValue);
                      }}
                    />
                  </div>
                </div>
                <div className={classes.detail_description}>
                  <p>
                    {!loaded ? payload[0]?.partner?.description : ""}
                  </p>
                </div>
              </div>
            </div>
            <div className="col-md-9">
              <div className={classes.detail_row}>
                {!loaded ? payload.map((item, index) => {
                  return <div className={classes.detail_col} key={index}>
                    <div className={classes.detail_card}>
                      <h4>{item?.name?.length > 44 ? item?.name.slice(0, 44) : item?.name}</h4>
                      <p>{item?.description?.length > 100 ? item?.description.slice(0, 96) : item?.description}...</p>
                      <a href={item?.gotoLink} target="_blank" >Открыть предолжение</a>
                    </div>
                  </div>
                }) : ""}
              </div>
            </div>
          </div> :

          <div className="container">
            <h1>404 Not Found</h1>
          </div>
        }
      </div>
      {
        loaded ? <div className={classes.loader}>
          <CircularProgress color="inherit" />
        </div> : ""
      }
    </div>
  )
}
