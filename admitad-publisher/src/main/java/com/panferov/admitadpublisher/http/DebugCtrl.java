package com.panferov.admitadpublisher.http;

import com.panferov.admitadpublisher.service.publishers.CouponPublisher;
import com.panferov.admitadpublisher.service.publishers.PartnerPublisher;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@Slf4j
@RestController
@RequestMapping("/debug")
@RequiredArgsConstructor
public class DebugCtrl {

    private final CouponPublisher couponPublisher;
    private final PartnerPublisher partnerPublisher;

    @GetMapping("/coupon")
    public void couponTest() {
        couponPublisher.publish();
    }

    @GetMapping("/partner")
    public void partnerTest() {
        partnerPublisher.publish();
    }

}
