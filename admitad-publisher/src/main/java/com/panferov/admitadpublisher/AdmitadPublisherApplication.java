package com.panferov.admitadpublisher;

import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.PropertySource;

@EnableRabbit
@SpringBootApplication
@EnableCaching
@EnableFeignClients
@PropertySource(value = {"classpath:properties/admitad.yaml"})
public class AdmitadPublisherApplication {

	public static void main(String[] args) {
		SpringApplication.run(AdmitadPublisherApplication.class, args);
	}

}
