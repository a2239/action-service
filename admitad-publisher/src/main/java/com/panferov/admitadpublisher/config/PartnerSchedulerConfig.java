package com.panferov.admitadpublisher.config;

import lombok.Getter;
import org.springframework.stereotype.Component;

@Getter
@Component
public class PartnerSchedulerConfig {
    private int poolSize = 1;
    private String prefix = "PartnerScheduler";
    private int awaitTerminationSec = 300;
    private String partnerCron = "0 0 1 * * *";
 //   Only Test cron
  //  private String partnerCron = "* */1 * * * *";

}
