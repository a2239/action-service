package com.panferov.admitadpublisher.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule;
import lombok.RequiredArgsConstructor;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
@RequiredArgsConstructor
public class RabbitConfig {

    private final RabbitProperties properties;

    @Bean
    public Queue backendCouponQueue() {
        return new Queue(properties.getBackendCouponQueue(), true);
    }

    @Bean
    public Queue backendPartnerQueue() {
        return new Queue(properties.getBackendPartnerQueue(), true);
    }

    @Bean
    public Queue telegramCouponQueue() {
        return new Queue(properties.getTelegramCouponQueue(), true);
    }

    @Bean
    public Queue telegramPartnerQueue() {
        return new Queue(properties.getTelegramPartnerQueue(), true);
    }

    @Bean
    public FanoutExchange couponExchange() {
        return new FanoutExchange(properties.getCouponExchange());
    }

    @Bean
    public FanoutExchange partnerExchange() {
        return new FanoutExchange(properties.getPartnerExchange());
    }


    @Bean
    public Binding bindingCouponBackend(Queue backendCouponQueue, FanoutExchange couponExchange) {
        return BindingBuilder.bind(backendCouponQueue).to(couponExchange);
    }

    @Bean
    public Binding bindingCouponTelegram(Queue telegramCouponQueue, FanoutExchange couponExchange) {
        return BindingBuilder.bind(telegramCouponQueue).to(couponExchange);
    }

    @Bean
    public Binding bindingPartnerBackend(Queue backendPartnerQueue, FanoutExchange partnerExchange) {
        return BindingBuilder.bind(backendPartnerQueue).to(partnerExchange);
    }

    @Bean
    public Binding bindingPartnerTelegram(Queue telegramPartnerQueue, FanoutExchange partnerExchange) {
        return BindingBuilder.bind(telegramPartnerQueue).to(partnerExchange);
    }

    @Bean
    public Jackson2JsonMessageConverter producerJackson2MessageConverter() {
        ObjectMapper mapper = new ObjectMapper()
                .registerModule(new ParameterNamesModule())
                .registerModule(new Jdk8Module())
                .registerModule(new JavaTimeModule());
        return new Jackson2JsonMessageConverter(mapper);
    }

    @Bean
    public RabbitTemplate rabbitTemplate(final ConnectionFactory connectionFactory) {
        final RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
        rabbitTemplate.setMessageConverter(producerJackson2MessageConverter());
        return rabbitTemplate;
    }
}
