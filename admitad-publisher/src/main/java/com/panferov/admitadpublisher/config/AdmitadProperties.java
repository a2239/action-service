package com.panferov.admitadpublisher.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@Getter
public class AdmitadProperties {

    @Value("${client_id}")
    private String clientId;
    @Value("${client_secret}")
    private String clientSecret;
    @Value("${scope}")
    private String scope;
    @Value("${authorization_url}")
    private String authorizationUrl;
    @Value("${area_name}")
    private String areaName;
}
