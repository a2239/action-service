package com.panferov.admitadpublisher.model.admitad;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.panferov.admitadpublisher.config.WebsiteDeserializer;
import lombok.Data;

import java.util.List;

@Data
@JsonDeserialize(using = WebsiteDeserializer.class)
public class Websites {

    @JsonProperty
    private List<Website> websites;
}
