package com.panferov.admitadpublisher.model.admitad;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class Coupons {

    @JsonProperty(value = "results")
    private List<CouponDto> coupons;

}
