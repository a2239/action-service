package com.panferov.admitadpublisher.service;


import com.panferov.admitadpublisher.client.AdmitadContentClient;
import com.panferov.admitadpublisher.model.admitad.CouponDto;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
@AllArgsConstructor
public class CouponReceiver {

    private final AdmitadContentClient admitadContentClient;
    private final WebmasterWebsiteService webmasterWebsiteService;

    public List<CouponDto> getCouponsByIdPartner(Long id) {
        log.info("Receive Coupon from admitad");

        List<CouponDto> coupons = admitadContentClient.couponsForSite(
                webmasterWebsiteService.getWebsiteId("freeskidka_bot"), id)
                .getCoupons();

        return coupons;
    }
}