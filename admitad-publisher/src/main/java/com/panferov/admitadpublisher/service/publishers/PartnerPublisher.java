package com.panferov.admitadpublisher.service.publishers;


import com.google.common.collect.Lists;
import com.panferov.admitadpublisher.model.admitad.PartnerDto;
import com.panferov.admitadpublisher.service.PartnerReceiver;
import com.panferov.admitadpublisher.service.cache.PartnerCache;
import com.panferov.admitadpublisher.service.sender.PartnerSender;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Service
@Slf4j
@AllArgsConstructor
public class PartnerPublisher {

    private final PartnerReceiver partnerReceiver;
    private final PartnerSender sender;
    private final PartnerCache cache;

    public void publish() {
        log.info("Start Publish partner...");
        cache.clear();
        List<PartnerDto> admPartners = partnerReceiver.getPartners();
        List<List<PartnerDto>> batchPartners = Lists.partition(admPartners, 10);

        ExecutorService executor = Executors.newFixedThreadPool(5);
        for (List<PartnerDto> partners : batchPartners) {
            Runnable task = () -> single(partners);
            executor.execute(task);
        }
    }

    public void single(List<PartnerDto> admPartners) {
        log.info("Save Partner to cache");
        long start = System.currentTimeMillis();
        for (PartnerDto partner : admPartners) {
            cache.put(partner.getId(), partner);
            log.info("save partner - {}", partner.getId());
        }
        sender.send(admPartners);
        log.info("End save partners, spend time - {} sec", (((double) System.currentTimeMillis() - (double) start) / 1000L));
    }
}
