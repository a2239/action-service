package com.panferov.admitadpublisher.service.sender;

import com.panferov.admitadpublisher.config.RabbitProperties;
import com.panferov.admitadpublisher.model.admitad.PartnerDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class PartnerSender implements RabbitMQSender<PartnerDto> {

    private final RabbitTemplate rabbitTemplate;
    private final RabbitProperties properties;

    @Override
    public List<PartnerDto> send(List<PartnerDto> batch) {
        log.info("Sending message...");
        rabbitTemplate.setExchange(properties.getPartnerExchange());
        rabbitTemplate.convertAndSend(batch);
        return batch;
    }
}
