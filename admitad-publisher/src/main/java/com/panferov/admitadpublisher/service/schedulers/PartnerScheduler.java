package com.panferov.admitadpublisher.service.schedulers;

import com.panferov.admitadpublisher.config.PartnerSchedulerConfig;
import com.panferov.admitadpublisher.service.publishers.PartnerPublisher;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.stereotype.Service;

import java.util.concurrent.ScheduledExecutorService;

@Service
@Slf4j
@RequiredArgsConstructor
public class PartnerScheduler {

    private final PartnerSchedulerConfig config;
    private final PartnerPublisher partnerPublisher;
    private ThreadPoolTaskScheduler scheduler;

    public ScheduledExecutorService run() {
        log.info("Partners Update had been start");
        scheduler = new ThreadPoolTaskScheduler();
        scheduler.setPoolSize(config.getPoolSize());
        scheduler.setThreadNamePrefix(config.getPrefix());
        scheduler.setWaitForTasksToCompleteOnShutdown(true);
        scheduler.setAwaitTerminationSeconds(config.getAwaitTerminationSec());
        scheduler.initialize();

        var partnerCron = new CronTrigger(config.getPartnerCron());

        scheduler.schedule(partnerPublisher::publish, partnerCron);
        return scheduler.getScheduledExecutor();
    }
}
