package com.panferov.admitadpublisher.service.publishers;


import com.google.common.collect.Lists;
import com.panferov.admitadpublisher.model.admitad.CouponDto;
import com.panferov.admitadpublisher.model.admitad.PartnerDto;
import com.panferov.admitadpublisher.service.CouponReceiver;
import com.panferov.admitadpublisher.service.cache.PartnerCache;
import com.panferov.admitadpublisher.service.sender.CouponSender;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Service
@Slf4j
@AllArgsConstructor
public class CouponPublisher {

    private final CouponSender couponSender;
    private final CouponReceiver couponReceiver;
    private final PartnerCache partnerCache;

    public void publish() {

        List<List<PartnerDto>> batchPartners = Lists.partition(partnerCache.getPartners(), 10);

        ExecutorService executor = Executors.newFixedThreadPool(10);
        for (List<PartnerDto> partners : batchPartners) {
            Runnable task = () -> single(partners);
            executor.execute(task);
        }
    }

    public void single(List<PartnerDto> partners) {
        log.info("Start publish coupons");
        long start = System.currentTimeMillis();
        for (PartnerDto partner : partners) {
            List<CouponDto> couponsByIdPartner = couponReceiver.getCouponsByIdPartner(partner.getId());
            List<CouponDto> couponBatch = new ArrayList<>();
            for (int i = 0; i < couponsByIdPartner.size(); i++) {
                CouponDto coupon = couponsByIdPartner.get(i);
                coupon.setPartner(partner);
                couponBatch.add(coupon);
            }

            if (CollectionUtils.isNotEmpty(couponBatch)) {
                couponSender.send(couponBatch);
            }
        }
        log.info("End publish coupons, spend time - {} sec", (((double) System.currentTimeMillis() - (double) start) / 1000L));
    }
}