package com.project.tgdiscountservice.repository;

import com.project.tgdiscountservice.model.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryRepo extends JpaRepository<Category, Long> {

    @Query(value = "select tcat.* from telegram_partner tg\n" +
            "    left join telegram_coupon tc on tg.id = tc.partner_id\n" +
            "left join telegram_category tcat on tcat.partner_id = tg.id\n" +
            " where tc.date_end > now()", nativeQuery = true)
    List<Category> getCategoriesForCurrentCoupon();
}
