package com.project.tgdiscountservice.service.parser;

import com.project.tgdiscountservice.model.inner.InnerUpdate;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@Data
public class MessageParser extends Parser {

    private static final String TYPE_RESOLVER = "message";

    @Override
    public ParseData parse(InnerUpdate update) {
        log.info("MessageParser parse - {}", update);
        ParseData data = new ParseData();
        data.setTgMessage(update.getMessage());
        data.setChatId(data.getTgMessage().getChat().getId());

        String message = data.getTgMessage().getText();
        data.setCommand(message);

        if (message.contains("_")) {
            data.setSplit(message.split("_"));
            data.setId(data.getSplit()[1]);
            data.setCommand(data.getSplit()[0]);
        }
        return data;
    }

    @Override
    public String getCommand() {
        return TYPE_RESOLVER;
    }
}
