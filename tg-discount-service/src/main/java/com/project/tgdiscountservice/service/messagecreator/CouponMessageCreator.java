package com.project.tgdiscountservice.service.messagecreator;

import com.project.tgdiscountservice.model.Coupon;
import com.project.tgdiscountservice.model.TgPage;
import com.project.tgdiscountservice.model.inner.InnerUpdate;
import com.project.tgdiscountservice.service.CouponTextCreator;
import com.project.tgdiscountservice.service.KeyboardPageGeneration;
import com.project.tgdiscountservice.service.MessageSenderFacade;
import com.project.tgdiscountservice.service.parser.ParseData;
import com.project.tgdiscountservice.service.partner.PartnerService;
import com.project.tgdiscountservice.util.CouponUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class CouponMessageCreator implements MessageCreator {

    private final CouponTextCreator textCreator;
    private final PartnerService partnerRepo;
    private final KeyboardPageGeneration<Coupon> pageGeneration;
    private final MessageSenderFacade messageSenderFacade;
    private static final String TYPE_RESOLVER = "/cp";

    @Override
    public void create(InnerUpdate update, ParseData data) {
        log.info("CouponMessageCreator prepareMessage -{}, {}", update, data);

        String partnerId = data.getId();

        List<Coupon> coupons = new ArrayList<>(partnerRepo.findPartner(Long.valueOf(partnerId)).getCoupons());

        Coupon coupon = coupons.get(0);

        Coupon first = CouponUtil.create("<b>❗Первая страничка!</b>\n\nЛистайте стрелочками ⬅ ➡, находятся под сообщением снизу.", coupon);
        Coupon end = CouponUtil.create("<b>❗Последняя страничка!</b>\n\nЛистайте стрелочками ⬅ ➡, находятся под сообщением снизу.", coupon);

        coupons.add(0, first);
        coupons.add(end);

        int index = data.getIndex();
        String navigateCommand = data.getNavigateCommand();

        TgPage<Coupon> page = pageGeneration.getPage(
                coupons, index, navigateCommand, TYPE_RESOLVER, 1, partnerId);
        InlineKeyboardMarkup navigateKeyboard = page.getInlineKeyboardMarkup();
        List<Coupon> couponsList = page.getPage();


        for (int i = 0; i < couponsList.size(); i++) {
            Coupon couponPage = couponsList.get(i);
            StringBuilder message = textCreator.createText(couponPage);
            messageSenderFacade.sendMessage(update, message, navigateKeyboard);
        }
    }

    @Override
    public String getCommand() {
        return TYPE_RESOLVER;
    }
}
