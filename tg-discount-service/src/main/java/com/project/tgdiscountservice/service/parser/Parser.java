package com.project.tgdiscountservice.service.parser;

import com.project.tgdiscountservice.model.inner.InnerUpdate;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public abstract class Parser {

    public abstract ParseData parse(InnerUpdate update);

    public abstract String getCommand();

}
