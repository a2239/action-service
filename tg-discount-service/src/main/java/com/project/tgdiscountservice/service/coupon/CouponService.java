package com.project.tgdiscountservice.service.coupon;

import com.project.tgdiscountservice.model.Coupon;
import com.project.tgdiscountservice.repository.CouponRepo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;

@Slf4j
@RequiredArgsConstructor
@Service
public class CouponService {

    private final CouponRepo couponRepo;

    public Long save(Coupon coupon) {
        return couponRepo.save(coupon).getId();
    }

    public List<Coupon> saveAll(Collection<Coupon> coupons) {
        return couponRepo.saveAll(coupons);
    }

    public List<Coupon> findByAdmitadIds(Collection<Long> ids) {
        return couponRepo.findByAdmitadIds(ids);
    }
}
