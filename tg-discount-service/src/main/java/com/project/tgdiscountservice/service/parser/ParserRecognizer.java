package com.project.tgdiscountservice.service.parser;

import com.project.tgdiscountservice.model.inner.InnerUpdate;
import com.project.tgdiscountservice.service.parser.factories.ParserFactory;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class ParserRecognizer {

    private final ParserFactory factory;
    private final MessageParser messageParser;
    private final CallBackParser callBackParser;
    private final InlineQueryParser inlineQueryParser;

    public Parser recognize(InnerUpdate update) {
        log.info("ParserService parseUpdate - {}", update);

        //TODO about how it make better
        String messageType = "";
        if (update.getMessage() != null) {
            messageType = messageParser.getCommand();
        }
        if (update.getCallbackQuery() != null) {
            messageType = callBackParser.getCommand();
        }
        if (update.getInlineQuery() != null) {
            messageType = inlineQueryParser.getCommand();
        }

        return factory.getParserByCommand(messageType);
    }

}
