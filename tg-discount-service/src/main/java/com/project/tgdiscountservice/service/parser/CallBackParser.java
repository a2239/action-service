package com.project.tgdiscountservice.service.parser;

import com.project.tgdiscountservice.model.inner.InnerUpdate;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@Getter
public class CallBackParser extends Parser {

    private static final String TYPE_RESOLVER = "callback";

    @Override
    public ParseData parse(InnerUpdate update) {
        log.info("CallBackParser parse - {}", update);
        ParseData data = new ParseData();
        data.setCallbackQuery(update.getCallbackQuery());
        data.setCallBackData(update.getCallbackQuery().getData());
        data.setChatId(update.getCallbackQuery().getMessage().getChatId());
        data.setCommand(data.getCallBackData());

        if (data.getCommand().contains("_")) {
            data.setSplit(data.getCallBackData().split("_"));
            data.setCommand(data.getSplit()[0]);
            data.setNavigateCommand(data.getSplit()[1]);
            data.setIndex(Integer.parseInt(data.getSplit()[2]));
            if (update.getMessage() != null) {
                data.setId(data.getSplit()[1]);
            } else {
                data.setId(data.getSplit()[3]);
            }
        }
        return data;
    }

    @Override
    public String getCommand() {
        return TYPE_RESOLVER;
    }
}
