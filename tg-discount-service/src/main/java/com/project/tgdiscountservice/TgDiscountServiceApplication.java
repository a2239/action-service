package com.project.tgdiscountservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories
public class TgDiscountServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(TgDiscountServiceApplication.class, args);
    }

}
