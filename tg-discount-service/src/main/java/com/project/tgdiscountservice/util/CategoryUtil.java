package com.project.tgdiscountservice.util;

import com.project.tgdiscountservice.model.Category;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class CategoryUtil {

    public static Category create(String text) {
        var category = new Category();
        category.setId(-1L);
        category.setName(text);
        category.setAdmitadId(-1L);
        category.setLanguage("RU");
        return category;
    }
}
