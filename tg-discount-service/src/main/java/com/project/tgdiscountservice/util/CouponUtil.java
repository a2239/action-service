package com.project.tgdiscountservice.util;

import com.project.tgdiscountservice.model.Coupon;
import com.project.tgdiscountservice.model.Partner;
import com.project.tgdiscountservice.model.dto.CouponDto;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class CouponUtil {

    public static Coupon create(String text, Coupon couponIn) {
        log.info("CouponUtil create - {}, {}", text, couponIn);
        var coupon = new Coupon();
        coupon.setId(-1L);
        coupon.setName(text);
        coupon.setImageUrl(couponIn.getImageUrl());
        coupon.setPartner(couponIn.getPartner());
        return coupon;
    }

    public static Coupon create(String name, String description) {
        log.info("CouponUtil create - {}", name);
        var coupon = new Coupon();
        coupon.setId(-1L);
        coupon.setName(name);
        coupon.setDescription(description);
        return coupon;
    }
}
