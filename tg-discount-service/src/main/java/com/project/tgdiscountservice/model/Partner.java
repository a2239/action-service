package com.project.tgdiscountservice.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.search.engine.backend.types.TermVector;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.FullTextField;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.Indexed;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

@Indexed
@Getter
@Setter
@Entity
@NamedEntityGraphs({
        @NamedEntityGraph(name = "Partner[coupons, categories]", attributeNodes = {
                @NamedAttributeNode("coupons"),
                @NamedAttributeNode("categories")
        }),

        @NamedEntityGraph(name = "Partner[coupons]", attributeNodes = {
                @NamedAttributeNode("coupons")
        })
})
@Table(name = "telegram_partner")
public class Partner extends AbstractEntity {

    @Column(name = "admitad_id")
    private Long admitadId;

    @Column
    @FullTextField(termVector = TermVector.YES)
    private String name;

    @Column
    private String imageUrl;

    @OneToMany(mappedBy = "partner", cascade = {CascadeType.MERGE, CascadeType.REMOVE})
    @JsonManagedReference
    private List<Coupon> coupons;

    @Column(nullable = false)
    private LocalDateTime lastUpdate;

    @Column(nullable = false, updatable = false)
    private LocalDateTime createDate;

    @OneToMany(mappedBy = "partner", cascade = CascadeType.ALL)
    @JsonManagedReference
    private Set<Category> categories;

    @Column
    @FullTextField(termVector = TermVector.YES)
    private String description;

    @Column
    private Boolean exclusive;

    @Column
    private String image64;

    @PreUpdate
    private void update() {
        lastUpdate = LocalDateTime.now();
    }

    @PrePersist
    private void create() {
        final LocalDateTime now = LocalDateTime.now();
        lastUpdate = createDate = now;
    }

    public void addToCategory(Category category) {
        category.setPartner(this);
        this.categories.add(category);
    }
}
