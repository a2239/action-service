package com.project.tgdiscountservice.model.dto;

import lombok.Data;

@Data
public class CategoryDto {

    private Long id;

    private String name;

    private String language;

    private PartnerDto partner;
}
