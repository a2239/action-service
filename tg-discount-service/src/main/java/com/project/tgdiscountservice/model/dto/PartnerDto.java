package com.project.tgdiscountservice.model.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class PartnerDto {

    private Long id;

    private String name;

    private String image;

    private List<CategoryDto> categories;

    private String description;

    private Boolean exclusive;

}
