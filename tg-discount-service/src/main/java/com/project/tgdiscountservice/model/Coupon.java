package com.project.tgdiscountservice.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.search.engine.backend.types.TermVector;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.FullTextField;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.GenericField;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.Indexed;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Table(name = "telegram_coupon")
@Indexed
@Entity
@Getter
@Setter
@NamedEntityGraph(name = "Coupon[regions, partner]", attributeNodes = {
        @NamedAttributeNode("regions"),
        @NamedAttributeNode("partner")
})
public class Coupon extends AbstractEntity{

    @Column(name = "admitad_id")
    private Long admitadId;

    @Column
    @FullTextField(termVector = TermVector.YES)
    private String name;

    @Column
    private String status;

    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.REMOVE})
    @JsonBackReference
    private Partner partner;

    @Column
    @FullTextField(termVector = TermVector.YES)
    private String description;

    @BatchSize(size = 100)
    @ElementCollection(fetch = FetchType.LAZY)
    @CollectionTable(
            name = "telegram_region",
            joinColumns = @JoinColumn(name = "coupon_id")
    )
    @Column(name = "name")
    private List<String> regions;

    @Column
    private String discount;

    @Column
    private String species;

    @Column
    private String promocode;

    @Column
    private String framesetLink;

    @Column
    private String gotoLink;

    @Column
    @FullTextField(termVector = TermVector.YES)
    private String shortName;

    @Column
    @JsonProperty(value = "date_start")
    private LocalDateTime dateStart;

    @Column
    @GenericField
    private LocalDateTime dateEnd;

    @Column
    private String imageUrl;

    @Column(nullable = false)
    private LocalDateTime lastUpdate;

    @Column(nullable = false, updatable = false)
    private LocalDateTime createDate;

    @Column
    private String image64;

    @PreUpdate
    private void update() {
        lastUpdate = LocalDateTime.now();
    }

    @PrePersist
    private void create() {
        LocalDateTime now = LocalDateTime.now();
        lastUpdate = createDate = now;
    }
}
