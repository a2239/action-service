package com.project.tgdiscountservice.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Request {
    private String word;
}
