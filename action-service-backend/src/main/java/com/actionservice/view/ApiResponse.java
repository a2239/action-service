package com.actionservice.view;

import com.actionservice.view.GlobalControllerView.ApiResponseView;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonView;
import lombok.Data;

@Data
@JsonView(ApiResponseView.class)
public class ApiResponse<T> {
    @JsonView(ApiResponseView.class)
    private final boolean success;
    @JsonView(ApiResponseView.class)
    private final T payload;
    @JsonView(ApiResponseView.class)
    private final String message;
    @JsonCreator
    protected ApiResponse(@JsonProperty("success") boolean success,
                          @JsonProperty("payload") T payload, @JsonProperty("message") String message) {
        this.success = success;
        this.payload = payload;
        this.message = message;
    }
    public static <T> ApiResponse<T> ok(T payload) {
        return new ApiResponse<>(true, payload, null);
    }
    public static <T> ApiResponse<T> ok() {
        return new ApiResponse<>( true, null, null);
    }
    public static <T> ApiResponse<T> error(String message) {
        return new ApiResponse<>(false, null, message);
    }
    public static <T> ApiResponse<T> error(T payload, String message) {
        return new ApiResponse<>(false, payload, message);
    }
}