package com.actionservice.http;

import com.actionservice.repository.CouponRepository;
import com.actionservice.repository.PartnerRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("pictures")
public class PictureCtrl {

    private final PartnerRepository partnerRepository;
    private final CouponRepository couponRepository;

    @GetMapping(produces = MediaType.IMAGE_JPEG_VALUE, value = "/partner/{partnerId}")
    public @ResponseBody
    byte[] getPartnerPicture(@PathVariable Long partnerId) {
        String image64ByPartnerId = partnerRepository.findImage64ByPartnerId(partnerId);
        return Base64.getDecoder().decode(image64ByPartnerId.getBytes(StandardCharsets.UTF_8));
    }

    @GetMapping(produces = MediaType.IMAGE_JPEG_VALUE, value = "/coupon/{couponId}")
    public @ResponseBody
    byte[] getCouponPicture(@PathVariable Long couponId) {
        String image64ByPartnerId = couponRepository.findImage64ByCouponId(couponId);
        return Base64.getDecoder().decode(image64ByPartnerId.getBytes(StandardCharsets.UTF_8));
    }


}
