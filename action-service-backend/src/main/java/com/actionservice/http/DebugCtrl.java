package com.actionservice.http;

import com.actionservice.http.request.Request;
import com.actionservice.model.dto.CouponDto;
import com.actionservice.service.consumers.coupon.CouponService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@Slf4j
@RestController
@RequestMapping("/debug")
@RequiredArgsConstructor
public class DebugCtrl {

    private final CouponService searchService;

    @PostMapping("/search")
    public ResponseEntity<List<CouponDto>> findCouponByString(@RequestBody Request request) {
        log.info("CouponCtrl findCouponByString - {}", request.getWord());
        List<CouponDto> search = searchService.search(request);
        return new ResponseEntity<>(search, HttpStatus.OK);
    }

}
