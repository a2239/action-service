package com.actionservice.http;


import com.actionservice.model.dto.PartnerDto;
import com.actionservice.service.consumers.partner.PartnerService;
import com.actionservice.view.ApiResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/partners")
@RequiredArgsConstructor
public class PartnerCtrl {

    private final PartnerService partnerService;

    @GetMapping
    public ResponseEntity<ApiResponse<Page<PartnerDto>>> getAll(Pageable pageable) {
        Page<PartnerDto> partners = partnerService.findAll(pageable);
        return ResponseEntity.ok(ApiResponse.ok(partners));
    }

    @GetMapping(value = "/{categoryId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ApiResponse<List<PartnerDto>>> getPartnerByCategoryId(@PathVariable Long categoryId) {
        log.info("PartnerCtrl getPartnerByCategoryId - {}", categoryId);
        List<PartnerDto> partners = partnerService.findByCategoryId(categoryId);
        return ResponseEntity.ok(ApiResponse.ok(partners));
    }

    @GetMapping("/search")
    public ResponseEntity<List<PartnerDto>> findCouponByString(String searchString) {
        log.info("PartnerCtrl findCouponByString - {}", searchString);
        List<PartnerDto> search = partnerService.search(searchString);
        return new ResponseEntity<>(search, HttpStatus.OK);
    }

}
