package com.actionservice.http;

import com.actionservice.http.request.Request;
import com.actionservice.model.dto.CouponDto;
import com.actionservice.service.consumers.coupon.CouponService;
import com.actionservice.view.ApiResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/coupons")
@RequiredArgsConstructor
public class CouponCtrl {

    private final CouponService couponService;

    @GetMapping("partner/{partnerId}")
    public ResponseEntity<ApiResponse<List<CouponDto>>> getCouponsByPartnerId(@PathVariable Long partnerId) {
        log.info("CouponCtrl getCouponsByPartnerId, partnerId - {}", partnerId);
        List<CouponDto> couponByPartnerId = couponService.findCouponByPartnerId(partnerId);
        return ResponseEntity.ok(ApiResponse.ok(couponByPartnerId));
    }

    @GetMapping
    public ResponseEntity<ApiResponse<Page<CouponDto>>> getCoupons(Pageable pageable) {
        log.info("CouponCtrl getCouponsByPartnerId");
        Page<CouponDto> coupons = couponService.findAllAtTheMoment(pageable);
        return ResponseEntity.ok(ApiResponse.ok(coupons));
    }

    @GetMapping("/{couponId}")
    public ResponseEntity<ApiResponse<CouponDto>> getCouponsById(@PathVariable Long couponId) {
        log.info("CouponCtrl getCouponsByPartnerId");
        CouponDto coupon = couponService.findCouponById(couponId);
        return ResponseEntity.ok(ApiResponse.ok(coupon));
    }

    @GetMapping("/search")
    public ResponseEntity<ApiResponse<List<CouponDto>>> findCouponByString(String word) {
        log.info("CouponCtrl findCouponByString - {}", word);
        Request request = new Request();
        request.setWord(word);
        List<CouponDto> search = couponService.search(request);
        return ResponseEntity.ok(ApiResponse.ok(search));
    }
}
