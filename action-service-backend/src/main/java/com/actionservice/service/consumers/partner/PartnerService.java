package com.actionservice.service.consumers.partner;


import com.actionservice.model.Category;
import com.actionservice.model.Coupon;
import com.actionservice.model.Partner;
import com.actionservice.model.dto.CategoryDto;
import com.actionservice.model.dto.CouponDto;
import com.actionservice.model.dto.PartnerDto;
import com.actionservice.repository.CategoryRepository;
import com.actionservice.repository.CouponRepository;
import com.actionservice.repository.PartnerRepository;
import com.actionservice.service.search.PartnerSearch;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class PartnerService {

    private final PartnerRepository partnerRepo;
    private final CategoryRepository categoryRepository;
    private final CouponRepository couponRepository;
    private final PartnerSearch partnerSearch;

    public PartnerDto findPartner(Long id) {
        return toDto(partnerRepo.findById(id)
                .orElseThrow(RuntimeException::new));
    }

    public Partner save(Partner partner) {
        return partnerRepo.save(partner);
    }

    public List<PartnerDto> findByCategoryId(Long categoryId) {
        return partnerRepo.findByCategoryId(categoryId)
                .stream()
                .map(PartnerService::toDto)
                .collect(Collectors.toList());
    }

    public Page<PartnerDto> findAll(Pageable pageable) {

        List<PartnerDto> partners = partnerRepo.findAll(pageable)
                .stream()
                .map(partner -> {
                    partner.setCategories(new HashSet<>(categoryRepository.findAllById(List.of(partner.getId()))));
                    partner.setCoupons(couponRepository.findAllById(List.of(partner.getId())));
                    return PartnerService.toDto(partner);
                }).collect(Collectors.toList());

        return new PageImpl<>(partners);
    }

    public List<Partner> findByAdmitadIds(Set<Long> ids) {
        return partnerRepo.findByAdmitadIds(ids);
    }

    public Optional<Partner> findByAdmitadId(Long id) {
        return partnerRepo.findByAdmitadId(id);
    }

    public List<Partner> saveAll(Collection<Partner> partners) {
        return partnerRepo.saveAll(partners);
    }

    public List<PartnerDto> search(String searchString) {
        return search(searchString);
    }

    private static PartnerDto toDto(Partner partner) {
        var dto = new PartnerDto();
        dto.setId(partner.getId());
        dto.setAdmitadId(partner.getAdmitadId());
        dto.setName(partner.getName());
        dto.setImageUrl(partner.getImageUrl());
        dto.setLastUpdate(partner.getLastUpdate());

        Set<CategoryDto> categories = partner.getCategories()
                .stream()
                .map(PartnerService::toDto)
                .collect(Collectors.toSet());

        dto.setCategories(categories);
        dto.setDescription(partner.getDescription());
        dto.setExclusive(partner.getExclusive());
        dto.setImage64(partner.getImage64());
        return dto;
    }

    private static CouponDto toDto(Coupon coupon) {
        var dto = new CouponDto();
        dto.setId(coupon.getId());
        dto.setAdmitadId(coupon.getAdmitadId());
        dto.setName(coupon.getName());
        dto.setStatus(coupon.getStatus());
        dto.setDescription(coupon.getDescription());
        dto.setRegions(coupon.getRegions());
        dto.setDiscount(coupon.getDiscount());
        dto.setSpecies(coupon.getSpecies());
        dto.setPromocode(coupon.getPromocode());
        dto.setFramesetLink(coupon.getFramesetLink());
        dto.setGotoLink(coupon.getGotoLink());
        dto.setShortName(coupon.getShortName());
        dto.setDateStart(coupon.getDateStart());
        dto.setDateEnd(coupon.getDateEnd());
        dto.setImageUrl(coupon.getImageUrl());
        dto.setLastUpdate(coupon.getLastUpdate());
        dto.setImage64(coupon.getImage64());
        return dto;
    }

    private static CategoryDto toDto(com.actionservice.model.Category category) {
        CategoryDto dto = new CategoryDto();
        dto.setId(category.getId());
        dto.setAdmitadId(category.getAdmitadId());
        dto.setName(category.getName());
        dto.setLanguage(category.getLanguage());
        return dto;
    }


}
