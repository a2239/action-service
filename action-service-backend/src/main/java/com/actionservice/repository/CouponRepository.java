package com.actionservice.repository;

import com.actionservice.model.Coupon;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;

@Repository
public interface CouponRepository extends JpaRepository<Coupon, Long> {

    @EntityGraph(value = "Coupon[regions, partner]")
    @Query("select c from Coupon c")
    List<Coupon> findAll();


    @EntityGraph(value = "Coupon[regions, partner]")
    @Query("select c from Coupon c where c.dateEnd > :dateNow or c.dateEnd is null")
    Page<Coupon> findAllAtTheMoment(@Param("dateNow") LocalDateTime dateNow, Pageable pageable);

    @Modifying
    @Transactional
    @Query("update Coupon c set c = :coupon where c.admitadId = :id")
    void update(Coupon coupon, Long id);

    @EntityGraph(value = "Coupon[regions, partner]")
    @Query("select c from Coupon c where c.partner.id = :partnerId")
    List<Coupon> findCouponByPartnerId(Long partnerId);

    @Query("select c.image64 from Coupon c where c.id = :couponId")
    String findImage64ByCouponId(@Param("couponId") Long couponId);

    @EntityGraph(value = "Coupon[regions, partner]")
    @Query("select c from Coupon c where c.admitadId in :admitadIds")
    List<Coupon> findByAdmitadIds(@Param("admitadIds") Collection<Long> admitadIds);

}
