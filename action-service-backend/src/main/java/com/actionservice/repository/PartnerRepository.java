package com.actionservice.repository;

import com.actionservice.model.Partner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Repository
public interface PartnerRepository extends JpaRepository<Partner, Long> {

    Page<Partner> findAll(Pageable pageable);

    @Modifying
    @Query("update Partner p set p = :partner where p.admitadId = :id")
    @Transactional
    void update(Partner partner, Long id);

    @Query("select e from Category c left join c.partner e where c.admitadId = :admitadId")
    List<Partner> findByCategoryId(@Param("admitadId") Long admitadId);

    @Query("select p.image64 from Partner p where p.id = :partnerId")
    String findImage64ByPartnerId(@Param("partnerId") Long partnerId);

    @EntityGraph("Partner[coupons,categories]")
    @Query("select p from Partner p where p.admitadId in (:admitadIds) ")
    List<Partner> findByAdmitadIds(@Param("admitadIds") Set<Long> admitadIds);

    @Query("select p from Partner p")
    List<Partner> findAllWithoutChildEntity();

    @Query("select p from Partner p where p.admitadId = :id")
    Optional<Partner> findByAdmitadId(Long id);

}
